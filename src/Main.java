import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.core.Profile;
import jade.wrapper.*;

public class Main {

	public static void main(String[] args) {
		Runtime runtime = Runtime.instance();
		Profile profile = new ProfileImpl();
		
		profile.setParameter(Profile.MAIN_HOST, "localhost");
		profile.setParameter(Profile.GUI, "true");
		
		ContainerController cc = runtime.createMainContainer(profile);
		AgentController ac;
		
		try {
			ac = cc.createNewAgent("Server", "agents.AgentServer", null);
			ac.start();	
		}
		catch(StaleProxyException e) {
			System.out.print(e.getMessage());
		}
	}
}
