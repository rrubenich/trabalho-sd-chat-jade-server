package agents;

import java.util.ArrayList;
import jade.core.Agent;
import jade.core.ContainerID;
import jade.domain.introspection.*;

public class AgentServer extends Agent{

	private static final long serialVersionUID = 1L;

	//Lista de Containers
	private ArrayList<ContainerID> containers = new ArrayList<ContainerID>();

	@Override
	protected void setup() {

		
		//AMSSubscriber ouve os eventos em containers
		AMSSubscriber subscriber = new AMSSubscriber(){

			private static final long serialVersionUID = 1L;

			@Override
			protected void installHandlers(java.util.Map handlers){
				
				//Quando um container é adicionado
				EventHandler addedHandler = new EventHandler(){
					public void handle(Event event){
						AddedContainer addedContainer = (AddedContainer) event;
						containers.add(addedContainer.getContainer());
					}
				};
				
				handlers.put(IntrospectionVocabulary.ADDEDCONTAINER,addedHandler);


				//Quando um container é removido
				EventHandler removedHandler = new EventHandler(){
					public void handle(Event event){
						RemovedContainer removedContainer = (RemovedContainer) event;
						ArrayList<ContainerID> temp = new ArrayList<ContainerID>(containers);
						for(ContainerID container : temp){
							if(container.getID().equalsIgnoreCase(removedContainer.getContainer().getID()))
								containers.remove(container);
						}
					}
				};
				
				handlers.put(IntrospectionVocabulary.REMOVEDCONTAINER,removedHandler);
			}


		};
		addBehaviour(subscriber);
	}
}
